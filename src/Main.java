
import java.util.Date;
import java.util.Scanner;

public class Main extends Sales {

    Scanner fr = new Scanner(System.in);
    String menu[] = {"1.Chicken Burger", "2.Beef Burger", "3.Chicken hotdog", "4.Beef hotdog", "5.Checkout"};

    public Main(int chickenPatty, int beefPatty, int chickenBurger, int beefBurger, int chickenHotdog, int beefHotdog, int amount1, int amount2, int amount3, int amount4) {
        super(chickenBurger, beefBurger, chickenHotdog, beefHotdog, amount1, amount2, amount3, amount4);
    }


    public void display() {

        try {
            System.out.print("""
                    Functions:
                    1. Order
                    2. End the day
                    """);
            System.out.print("Please choose the function (1/2): ");
            int selected = fr.nextInt();

            switch (selected) {
                case 1 -> {
                    for (int i = 0; i < 1; i--) {
                        try {
                            System.out.print("Ordering Menu:\n");
                            for (String s : menu) {
                                System.out.println(s);
                            }
                            System.out.print("Please choose your option (1 - 5): ");
                            int select = fr.nextInt();
                            if (select == 1) {
                                System.out.print("How many Chicken burger?: ");
                                int amount = fr.nextInt();
                                if (getChickenPatty() >= amount && getRoundBun() >= amount) {
                                    int amounts = amount + getChickenBurger();
                                    setAmount1(amounts + getAmount1());
                                    setChickenBurger(amounts);
                                    setTotalOder((amount * 6) + getTotalOder());
                                    setChickenPatty(getChickenPatty() - amount);
                                    setRoundBun(getRoundBun() - amount);
                                } else {
                                    System.out.println("out of stock");
                                }
                                System.out.println("==========================");

                            } else if (select == 2) {
                                System.out.print("How many Beef Burger?: ");
                                int amount = fr.nextInt();
                                if (getBeefPatty() >= amount && getRoundBun() >= amount) {
                                    int amounts = amount + getBeefBurger();
                                    setAmount2(amounts + getAmount2());
                                    setBeefBurger(amounts);
                                    setTotalOder((amount * 6.50) + getTotalOder());
                                    setBeefPatty(getBeefPatty() - amount);
                                    setRoundBun(getRoundBun() - amount);
                                } else {
                                    System.out.println("out of stock");

                                }
                                System.out.println("==========================");

                            } else if (select == 3) {
                                System.out.print("How many Chicken hotdog?: ");
                                int amount = fr.nextInt();
                                if (getChickenSausage() >= amount && getLongBun() >= amount) {
                                    int amounts = amount + getChickenHotdog();
                                    setAmount3(amounts + getAmount3());
                                    setChickenHotdog(amounts);
                                    setTotalOder((amount * 5.5) + getTotalOder());
                                    setChickenSausage(getChickenSausage() - amount);
                                    setLongBun(getLongBun() - amount);
                                } else {
                                    System.out.println("out of stock");
                                }
                                System.out.println("==========================");

                            } else if (select == 4) {
                                System.out.print("How many Beef hotdog?: ");
                                int amount = fr.nextInt();
                                if (getBeefSausage() >= amount && getLongBun() >= amount) {
                                    System.out.println("out of stock");
                                    int amounts = amount + getBeefHotdog();
                                    setAmount4(amounts + getAmount4());
                                    setBeefHotdog(amounts);
                                    setTotalOder((amount * 5.80) + getTotalOder());
                                    setBeefSausage(getBeefSausage() - amount);
                                    setLongBun(getLongBun() - amount);
                                } else {
                                    System.out.println("out of stock");
                                }
                                System.out.println("==========================");


                            } else if (select == 5) {
                                String expected = "\t\t\t\tReceipt\n"
                                                  + "---------------------------------------------\n"
                                                  + new Date() + "\n"
                                                  + "Item\t\t\t\tQuantity\t\tSubtotal\n"
                                                  + "Chicken Burger(6.0)\t\t" + getChickenBurger() + "\t\t\t" + getChickenBurger() * 6.0 + "\n"
                                                  + "Beef Burger(6.5)\t\t" + getBeefBurger() + "\t\t\t" + getBeefBurger() * 6.5 + "\n"
                                                  + "Chicken Hotdog(5.5)\t\t" + getChickenHotdog() + "\t\t\t" + getChickenHotdog() * 5.5 + "\n"
                                                  + "Beef Hotdog(5.8)\t\t" + getBeefHotdog() + "\t\t\t" + getBeefHotdog() * 5.8 + "\n"
                                                  + "Total\t\t\t\t\t\t\t\t" + getTotalOder() + "\n"
                                                  + "==============================================\n"
                                                  + "\t\t\t\tThank you.\n";
                                System.out.println(expected);
                                // clear values display of order
                                setTotal(getTotalOder() + getTotal());
                                setChickenBurger(0);
                                setBeefBurger(0);
                                setChickenHotdog(0);
                                setBeefHotdog(0);
                                setTotalOder(0);

                                break;
                            } else {
                                System.out.println("Wrong option, please enter a valid option (1-5)");
                            }

                        } catch (Exception ex) {
                            System.out.println("Please enter number");
                        }
                    }
                }
                case 2 -> {
                    System.out.println("1. Enter the raw material quantity:\n" +
                                       "2. Order and material quantity");
                    System.out.print("Please choose the function (1/2): ");
                    int pickOrder = fr.nextInt();
                    switch (pickOrder) {

                        case 1 -> {
                            System.out.print("Chicken patty :");
                            int chickenPatty = fr.nextInt();
                            setChickenPatty(chickenPatty);
                            System.out.print("Beef patty :");
                            int beefPatty = fr.nextInt();
                            setBeefPatty(beefPatty);
                            System.out.print("Chicken sausage :");
                            int chickenSausage = fr.nextInt();
                            setChickenSausage(chickenSausage);
                            System.out.print("Beef sausage :");
                            int beefSausage = fr.nextInt();
                            setBeefSausage(beefSausage);
                            System.out.print("Round bun :");
                            int roundBun = fr.nextInt();
                            setRoundBun(roundBun);
                            System.out.print("Long bun :");
                            int longBun = fr.nextInt();
                            setLongBun(longBun);
                        }
                        case 2 -> {

                            String sales = "--------------------------------------------------\n"
                                           + "Item\t\t\t\tQuantity sold\t\tSubtotal(RM)\t\n"
                                           + "--------------------------------------------------\n"
                                           + "Chicken Burger(6.0)\t\t" + getAmount1() + "\t\t\t" + getAmount1() * 6.0 + "\n"
                                           + "Beef Burger(6.5)\t\t" + getAmount2() + "\t\t\t" + getAmount2() * 6.5 + "\n"
                                           + "Chicken Hotdog(5.5)\t\t" + getAmount3() + "\t\t\t" + getAmount3() * 5.5 + "\n"
                                           + "Beef Hotdog(5.8)\t\t" + getAmount4() + "\t\t\t" + getAmount4() * 5.8 + "\n"
                                           + "Total\t\t\t\t\t\t\t\t" + getTotal() + "\n"
                                           + "==================================================\n";

                            String item = "Raw material left:\n"
                                          + "---------------------------------------\n"
                                          + "Item\t\t\tQuantity left\n"
                                          + "---------------------------------------\n"
                                          + "Chicken patty\t\t" + getChickenPatty() + "\n"
                                          + "Beef patty\t\t\t" + getBeefPatty() + "\n"
                                          + "Chicken sausage\t\t" + getChickenSausage() + "\n"
                                          + "Beef sausage\t\t" + getBeefSausage() + "\n"
                                          + "Round bun\t\t\t" + getRoundBun() + "\n"
                                          + "Long bun\t\t\t" + getLongBun() + "\n"
                                          + "---------------------------------------\n";

                            System.out.println(sales);
                            System.out.println(item);
                        }
                    }
                }
                default -> System.out.print("Wrong option, please enter a valid option (1/2)");
            }

        } catch (Exception ex) {
            System.out.println("Please enter number");

        }
    }


    public static void main(String[] args) {
        int chickenPatty = 0;
        int beefPatty = 0;
        int chickenSausage = 0;
        int beefSausage = 0;
        int roundBun = 0;
        int longBun = 0;
        int amount1 = 0;
        int amount2 = 0;
        int amount3 = 0;
        int amount4 = 0;
        Main main = new Main(chickenPatty, beefPatty, chickenSausage, beefSausage, roundBun, longBun, amount1, amount2, amount3, amount4);
        boolean x = true;
        while (true) {
            if (x) {
                try {
                    main.display();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
