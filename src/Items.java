public class Items {

    private int chickenPatty;
    private int beefPatty;
    private int chickenSausage;
    private int beefSausage;
    private int roundBun;
    private int longBun;

    public int getChickenPatty() {
        return chickenPatty;
    }

    public void setChickenPatty(int chickenPatty) {
        this.chickenPatty = chickenPatty;
    }

    public int getBeefPatty() {
        return beefPatty;
    }

    public void setBeefPatty(int beefPatty) {
        this.beefPatty = beefPatty;
    }

    public int getChickenSausage() {
        return chickenSausage;
    }

    public void setChickenSausage(int chickenSausage) {
        this.chickenSausage = chickenSausage;
    }

    public int getBeefSausage() {
        return beefSausage;
    }

    public void setBeefSausage(int beefSausage) {
        this.beefSausage = beefSausage;
    }

    public int getRoundBun() {
        return roundBun;
    }

    public void setRoundBun(int roundBun) {
        this.roundBun = roundBun;
    }

    public int getLongBun() {
        return longBun;
    }

    public void setLongBun(int longBun) {
        this.longBun = longBun;
    }
}
