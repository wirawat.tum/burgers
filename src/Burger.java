public class Burger extends Items{

    private int chickenBurger;
    private int BeefBurger;
    private int ChickenHotdog;
    private int BeefHotdog;
    private int amount1;
    private int amount2;
    private int amount3;
    private int amount4;

    public Burger(int chickenBurger, int beefBurger, int chickenHotdog, int beefHotdog, int amount1, int amount2, int amount3, int amount4) {
        this.chickenBurger = chickenBurger;
        BeefBurger = beefBurger;
        ChickenHotdog = chickenHotdog;
        BeefHotdog = beefHotdog;
        this.amount1 = amount1;
        this.amount2 = amount2;
        this.amount3 = amount3;
        this.amount4 = amount4;
    }

    public int getAmount1() {
        return amount1;
    }

    public void setAmount1(int amount1) {
        this.amount1 = amount1;
    }

    public int getAmount2() {
        return amount2;
    }

    public void setAmount2(int amount2) {
        this.amount2 = amount2;
    }

    public int getAmount3() {
        return amount3;
    }

    public void setAmount3(int amount3) {
        this.amount3 = amount3;
    }

    public int getAmount4() {
        return amount4;
    }

    public void setAmount4(int amount4) {
        this.amount4 = amount4;
    }



    public int getChickenHotdog() {
        return ChickenHotdog;
    }

    public void setChickenHotdog(int chickenHotdog) {
        ChickenHotdog = chickenHotdog;
    }

    public int getBeefHotdog() {
        return BeefHotdog;
    }

    public void setBeefHotdog(int beefHotdog) {
        BeefHotdog = beefHotdog;
    }

    public int getChickenBurger() {
        return chickenBurger;
    }

    public void setChickenBurger(int chickenBurger) {
        this.chickenBurger = chickenBurger;
    }

    public int getBeefBurger() {
        return BeefBurger;
    }

    public void setBeefBurger(int beefBurger) {
        BeefBurger = beefBurger;
    }
}
