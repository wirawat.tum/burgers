public class Sales extends Burger {

    private double total;
    private double totalOder;

    public Sales(int chickenBurger, int beefBurger, int chickenHotdog, int beefHotdog, int amount1, int amount2, int amount3, int amount4) {
        super(chickenBurger, beefBurger, chickenHotdog, beefHotdog, amount1, amount2, amount3, amount4);
    }


    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getTotalOder() {
        return totalOder;
    }

    public void setTotalOder(double totalOder) {
        this.totalOder = totalOder;
    }
}
